package scripts.fluffeescrafter;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.fluffeesBanking.FluffeesBanking;
import scripts.fluffeesBanking.FluffeesBankingBuilder;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.scripting.data.Bag;
import scripts.fluffeesapi.scripting.data.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.mission.missionTypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionManagerScript;
import scripts.fluffeesapi.scripting.javafx.utilities.GUI;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.furnacecrafting.FurnaceCrafting;
import scripts.furnacecrafting.data.Furnaces;
import scripts.furnacecrafting.data.items.Rings;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Crafting",
        name        = "Fluffees Crafter",
        description = "Well, it crafts things.",
        gameMode = 1)

public class FluffeesCrafterScript extends MissionManagerScript implements Painting, Starting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(ScriptPaint.hex2Rgb("#a1e8af"), "Crafter")
            .addField("Version", Double.toString(1.00))
            .build();
    private LinkedList<Mission> missionList = new LinkedList<>();

    @Override
    public GUI getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }

    @Override
    public Queue<Mission> getMissions() {
        return missionList;
    }

    @Override
    public boolean shouldLoopMissions() {
        return true;
    }

    @Override
    public boolean isMissionValid() {
        return true;
    }

    @Override
    public boolean isMissionCompleted() {
        return false;
    }

    @Override
    public ArrayList<AbstractMap.SimpleEntry<String, String>> getPaintFields() {
        ArrayList<AbstractMap.SimpleEntry<String, String>> fields = super.getPaintFields();
        fields.add(0, new AbstractMap.SimpleEntry<>("Mission", BagSingleton.getInstance().get("missionName", "Loading")));
        return fields;
    }

    @Override
    public void onStart() {
        missionList.add(new FluffeesBankingBuilder()
                .withDepositAllEquipment()
                .withDepositOtherItems()
                .withWithdrawalItems(new InteractableBankItem.Builder("Sapphire", 13).build())
                .withWithdrawalItems(new InteractableBankItem.Builder("Gold bar", 13).build())
                .withWithdrawalItems(new InteractableBankItem.Builder("Ring mould", 1).build())
                .build());
        missionList.add(new FurnaceCrafting(Rings.SAPPHIRE_RING, Furnaces.EDGEVILLE_FURNACE)
        .setPostMissionFunction(() -> BagSingleton.getInstance().addOrUpdate("shouldBank", true)));
        super.onStart();
    }
}
